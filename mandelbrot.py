#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 21:05:56 2017

@author: BabeLu
"""

# import numpy
import numpy as np
import matplotlib.pyplot as plt
# construct a grid of c=x+1j*y values in range [-2,1]*[-1.5,1.5]
N_max = 50
some_threshold = 50
y, x = np.ogrid[-1.5:1.5:199j, -2:1:199j]
mask = np.zeros((199,199))
#Do the iteration: if c is in the mandelbrot set, then the corresponding mask is 1; if c is NOT in the mandelbrot set, then the correponding mask is 0.
for i in range(199):
    for j in range(199):
        c = x[0,i] + y[j,0]*1j
        z = 0
        inSet = 1
        for iters in range(N_max):
            z = z**2 + c
            if abs(z)>some_threshold:
                inSet = 0
                break
        if inSet==1:
            mask[i,j]=1
# set up coordinates and do plotting
plt.imshow(mask.T, extent=[-2,1,-1.5,1.5]) # ,cmap=cm.jet
plt.gray()
plt.savefig('mandelbrot.png',dpi=600)
